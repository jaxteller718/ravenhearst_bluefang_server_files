This Modlet can exist on client and server side; however, through code, it will not run on client machines.

When this Harmony patch is ran on dedicated servers, it converts, on the fly, any ShapeModelEntity's model to use a vanilla place holder.

Likewise, when an entity is loading a custom material swap, it will be removed on the fly.